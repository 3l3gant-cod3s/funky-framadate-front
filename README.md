# Framadate - funky version
![landing_page](docs/img/landing_page.png)
<a href="https://weblate.framasoft.org/engage/funky-framadate/">
<img src="https://weblate.framasoft.org/widgets/funky-framadate/-/funky-framadate-front/multi-auto.svg" alt="État de la traduction" />
</a>

<a href="https://weblate.framasoft.org/engage/funky-framadate/">
<img src="https://weblate.framasoft.org/widgets/funky-framadate/-/funky-framadate-front/open-graph.png" alt="État de la traduction" />
</a>

FR: Un logiciel libre de sondage fait par les contributeurs de l'association Framasoft, avec une API backend.

EN: A libre polling software made by contributors around the French association Framasoft.
This version uses a brand new backend API.

 - l'API de backend est en PHP / Symfony, ses sources disponible sur framagit https://framagit.org/tykayn/date-poll-api

## Pour débuter - getting started
[lire la doc pour débuter votre Funky Framadate](docs/GETTING_STARTED.md)

## Design et maquettes
Maquette naviguable: https://www.figma.com/file/Q3I15MEuOtTwL0WIQ2eSMu/%F0%9F%93%85-Framadate?node-id=0%3A6510


## Documentation
FR: Toute la documentation est disponible [dans le dossier "doc"](/docs), principalement en Français.
EN: All documentation is available in the "doc" folder, mainly in French because reasons.


# Version funky framadate

* Selon les maquettes par Thomas Bonamy : https://www.figma.com/file/Q3I15MEuOtTwL0WIQ2eSMu
* icones https://feathericons.com/
	
* [Spécifications](docs/cadrage/specifications-fonctionnelles)
* maquettes par @maiwann : https://scene.zeplin.io/project/5d4d83d68866d6522ff2ff10
* vidéo de démo des maquettes par @maiwann : https://nuage.maiwann.net/s/JRRHTR9D2akMAa7
* discussions sur framateam canal général : https://framateam.org/ux-framatrucs/channaels/framadate
* discussions techniques côté développeurs : https://framateam.org/ux-framatrucs/channels/framadate-dev
* [notes de réunion](docs/reunions)
* [traductions](docs/traductions)


## Screenshots - exemples de maquette de la nouvelle version
![funky_framadate_maquette](docs/img/Screenshot_2021-04-19%20migration%20depuis%20un%20Framadate%20version%201.jpeg)
![funky_framadate_maquette](docs/img/advanced_config.png)
![funky_framadate_maquette](docs/img/preview_creation.png)
![funky_framadate_maquette](docs/img/kind_poll.png)
![funky_framadate_maquette](docs/img/colors_framadate_funky.png)
![funky_framadate_maquette](docs/img/framdate_funky_design.png)

# Documentations sur Angular

* `{- sur sass -}` (on va utiliser CSS, si angular permet d'avoir des variables CSS, @newick)


## LIBRARIES USED

|     status      | lib choice_label                                                       | usage                                                     |
| :-------------: | -------------------------------------------------------------- | --------------------------------------------------------- |
|                 | [axios](https://github.com/axios/axios)                        | http client                                               |
|                 | [bulma](https://bulma.io/)                                     | CSS framework                                             |
|                 | [chart.js](https://www.chartjs.org/)                           | PrimeNG solution for graphs. (Chart.js installs MomentJS) |
|                 | [compodoc](https://compodoc.app/)                              | Generate technic documentation                            |
|                 | ESlint, Prettier, Lint-staged                                  | Format & lint code                                        |
|                 | [fork-awesome](https://forkaweso.me)                           | Icons collection                                          |
|                 | [fullcalendar](https://fullcalendar.io/docs/initialize-es6)    | PrimeNG solution to manage & display calendars            |
|                 | [husky](https://www.npmjs.com/package/husky)                   | Hook actions on commit                                    |
|                 | [jest](https://jestjs.io/)                                     | test engine                                               |
|                 | [json-server](https://www.npmjs.com/package/json-server)       | local server for mocking data backend                     |
|     removed     | [locale-enum](https://www.npmjs.com/package/locale-enum)       | enum of all locales                                       |
|                 | [momentJS](https://momentjs.com/)                              | manipulate dates. (chartJS’s dependency)                  |
| to be installed | [ng2-charts](https://valor-software.com/ng2-charts/)           | Manipulate graphs along with chart.js                     |
|                 | [ngx-clipboard](https://www.npmjs.com/package/ngx-clipboard)   | Handle clipboard                                          |
|                 | [ngx-markdown](https://www.npmjs.com/package/ngx-markdown)     | markdown parser                                           |
|                 | [ngx-webstorage](https://www.npmjs.com/package/ngx-webstorage) | handle localStorage & webStorage                          |
|                 | [primeNG](https://www.primefaces.org/primeng/)                 | UI components collection                                  |
|                 | [quill](https://www.npmjs.com/package/quill)                   | powerful rich text editor. WYSIWYG.                       |
| to be installed | [short-uuid](https://www.npmjs.com/package/short-uuid)         | generate uuid                                             |
|     removed     | [storybook](https://storybook.js.org/)                         | StyleGuide UI                                             |
|                 | [ts-mockito](https://www.npmjs.com/package/ts-mockito)         | Mocks for testing.                                        |
|  to be removed  | [uuid](https://www.npmjs.com/package/uuid)                     | generate uuid                                             |

---
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.
