export interface DateChoice {
	literal: String;
	timeSlices: TimeSlices[];
	date_object: Date;
}

export interface TimeSlices {
	literal: string;
}

export interface PollAnswer {
	id: number;
	text: string;
	url: string;
	file: string;
	literal: string;
	date_object: Date;
	timeList: TimeSlices[];
}

const currentYear = new Date().getFullYear();
const currentMonth = new Date().getMonth();
const currentDay = new Date().getDate();

export const basicSlicesOfDay: TimeSlices[] = [
	{ literal: 'matin' },
	{ literal: 'midi' },
	{ literal: 'après-midi' },
	{ literal: 'soir' },
];
export const otherSlicesOfDay: TimeSlices[] = [
	{ literal: 'aux aurores' },
	{ literal: 'au petit dej' },
	{ literal: 'au deuxième petit dej des hobbits' },
];
export const defaultTimeOfDay: TimeSlices[] = (() => {
	return [...basicSlicesOfDay];
})();

export const otherTimeOfDay: TimeSlices[] = (() => {
	return [...otherSlicesOfDay];
})();
export const moreTimeOfDay: TimeSlices[] = (() => {
	return [...otherSlicesOfDay];
})();
export const defaultDates: DateChoice[] = [
	{
		literal: `${currentYear}-${currentMonth}-${currentDay}`,
		date_object: new Date(),
		timeSlices: defaultTimeOfDay,
	},
	{
		literal: `${currentYear}-${currentMonth}-${currentDay + 1}`,
		date_object: new Date(),
		timeSlices: defaultTimeOfDay,
	},
	{
		literal: `${currentYear}-${currentMonth}-${currentDay + 2}`,
		date_object: new Date(),
		timeSlices: defaultTimeOfDay,
	},
];

export const otherDefaultDates: DateChoice[] = [
	{
		literal: `${currentYear}-${currentMonth}-${currentDay}`,
		date_object: new Date(),
		timeSlices: defaultTimeOfDay,
	},
	{
		literal: `${currentYear}-${currentMonth}-${currentDay + 1}`,
		date_object: new Date(currentYear, currentMonth, currentDay + 1),
		timeSlices: otherTimeOfDay,
	},
	{
		literal: `${currentYear}-${currentMonth}-${currentDay + 2}`,
		date_object: new Date(),
		timeSlices: moreTimeOfDay,
	},
];
export const defaultAnswers: PollAnswer[] = [
	{
		id: 0,
		text: 'réponse de démo 1',
		file: '',
		url:
			'https://mastodon.cipherbliss.com/system/media_attachments/files/001/439/118/original/6fcf149bd902841b.png?1579471574',
		literal: `${currentYear}-${currentMonth}-${currentDay}`,
		date_object: new Date(),
		timeList: otherSlicesOfDay,
	},
	{
		id: 1,
		text: 'réponse 2',
		file: '',
		url:
			'https://mastodon.cipherbliss.com/system/media_attachments/files/001/439/118/original/6fcf149bd902841b.png?1579471574',
		literal: `${currentYear}-${currentMonth}-${currentDay + 1}`,
		date_object: new Date(),
		timeList: basicSlicesOfDay,
	},
	{
		id: 2,
		text: 'la réponse D',
		file: '',
		url:
			'https://mastodon.cipherbliss.com/system/media_attachments/files/001/439/118/original/6fcf149bd902841b.png?1579471574',
		literal: `${currentYear}-${currentMonth}-${currentDay + 2}`,
		date_object: new Date(),
		timeList: otherSlicesOfDay,
	},
];
