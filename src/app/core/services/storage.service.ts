import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Theme } from '../enums/theme.enum';
import { Stack } from '../models/stack.model';
import { Choice, ChoiceText } from '../models/choice.model';
import { Vote } from '../models/vote.model';
import { environment } from '../../../environments/environment';
import { DateChoice, defaultTimeOfDay, TimeSlices } from '../../../../mocks/old-stuff/config/defaultConfigs';
import { Poll } from '../models/poll.model';
import { Owner } from '../models/owner.model';
import { DateUtilitiesService } from './date.utilities.service';

@Injectable({
	providedIn: 'root',
})
export class StorageService {
	@LocalStorage()
	public theme: Theme;

	@LocalStorage()
	public language: string;

	@LocalStorage()
	public userPolls: Poll[] = [];

	@LocalStorage()
	public dateChoices: DateChoice[] = [];

	@LocalStorage()
	public timeSlices: TimeSlices[] = defaultTimeOfDay;

	@LocalStorage()
	public vote_stack: Stack = new Stack();

	/**
	 * choix de dates
	 */
	@LocalStorage()
	public choices: Choice[] = [];

	/**
	 * choix textes dans un sondage classique
	 */
	@LocalStorage()
	public choicesText: ChoiceText[] = [
		{
			name: '',
			url_display: '',
			url_href: '',
		},
	];

	constructor(public dateUtilities: DateUtilitiesService) {
		if (environment.autofill_participation) {
			this.userPolls.push(new Poll(new Owner(), 'Démo: Anniversaire de tonton Patrick', 'aujourdhui-ou-demain'));
			this.userPolls.push(new Poll(new Owner(), 'Démo: Atelier cuisine du quartier', 'aujourdhui-ou-demain'));
			this.userPolls.push(
				new Poll(new Owner(), 'Démo: Réunion du département des chatons', 'aujourdhui-ou-demain')
			);
		}
	}

	/**
	 * set default choices for the vote stack from the poll choices objects
	 * @param choices_list
	 */
	setChoicesForVoteStack(choices_list: Choice[]) {
		// change only if the poll custom_url changed or if there is no stack id for this poll

		if (!this.vote_stack.id) {
			this.vote_stack = new Stack();
			for (const choice of choices_list) {
				// if (environment.autofill_participation) {
				// 	console.log('autofill au hasard des votes à ce sondage');
				// 	this.toastService.display('autofill au hasard des votes à ce sondage');
				// 	const defaultvalue = Math.random() > 0.75 ? 'yes' : '';
				// 	this.vote_stack.votes.push(new Vote(timeSlice.id, defaultvalue));
				// } else {
				this.vote_stack.votes.push(new Vote(choice.id));
				// }
			}
		}
	}

	/**
	 * look for a timeSlice in the stored vote stack and change it answer
	 * @param choice_id
	 * @param value
	 */
	toggleAnswer(choice_id: number, value: string) {
		for (const vote of this.vote_stack.votes) {
			if (vote.choice_id == choice_id) {
				if (vote.value == value) {
					vote.value = '';
				} else {
					vote.value = value;
				}
			}
		}
	}

	/**
	 * check for the value of a timeSlice in the stored vote stack
	 * @param choice_id
	 * @param value
	 */
	choiceHasAnswerOfValue(choice_id: number, value: any) {
		for (const vote of this.vote_stack.votes) {
			if (vote.choice_id == choice_id) {
				if (vote.value == value) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * set all time slices of a timeSlice to the same answer at once
	 * @param groupe
	 * @param newAnswer
	 */
	setAllSubchoicesTo(groupe, newAnswer = 'yes') {
		groupe.choices.map((choice) => {
			for (const vote of this.vote_stack.votes) {
				if (vote.choice_id == choice.id) {
					vote.value = newAnswer;
				}
			}
		});
	}

	/**
	 * update vote stack from the backend
	 * @param voteStack
	 */
	mapVotes(voteStack: Stack) {
		console.log('mapVotes voteStack', voteStack);
		this.vote_stack = voteStack;
	}
}
