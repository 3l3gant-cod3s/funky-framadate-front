import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Theme } from '../../../../core/enums/theme.enum';
import { ThemeService } from '../../../../core/services/theme.service';

@Component({
	selector: 'app-theme-selector',
	templateUrl: './theme-selector.component.html',
	styleUrls: ['./theme-selector.component.scss'],
})
export class ThemeSelectorComponent implements OnInit {
	public themeEnum = Theme;
	public currentTheme: Observable<Theme> = this.themeService.theme;

	constructor(private themeService: ThemeService) {}

	ngOnInit(): void {}

	public selectTheme(theme: string): void {
		this.themeService.selectTheme(Theme[theme]);
	}
}
