import { Component, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'app-page-not-found',
	templateUrl: './page-not-found.component.html',
	styleUrls: ['./page-not-found.component.scss'],
})
export class PageNotFoundComponent {
	@Input()
	public message = 'PAGE_NOT_FOUND.DEFAULT';
	env: any = environment;
}
