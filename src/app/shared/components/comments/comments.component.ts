import { Component, Input } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import * as moment from 'moment';
import { Stack } from '../../../core/models/stack.model';
import { Poll } from '../../../core/models/poll.model';
import { ApiService } from '../../../core/services/api.service';
import { StorageService } from '../../../core/services/storage.service';
import { ToastService } from '../../../core/services/toast.service';

@Component({
	selector: 'app-comments',
	templateUrl: './comments.component.html',
	styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent {
	@Input() public vote_stack: Stack;
	@Input() public poll: Poll;

	public config: any = {
		myName: '',
		myEmail: '',
		myComment: '',
	};

	constructor(
		private pollService: PollService,
		private api: ApiService,
		private toastService: ToastService,
		private storageService: StorageService
	) {}

	calculateDaysAgoOfComment(dateAsString) {
		let numberOfDays = 0;

		if (dateAsString && dateAsString) {
			numberOfDays = moment(new Date()).diff(moment(new Date(dateAsString)), 'days');
		}

		return numberOfDays;
	}

	addComment() {
		this.pollService
			.addComment({
				pseudo: this.storageService.vote_stack.pseudo,
				text: this.storageService.vote_stack.comment,
			})
			.then((resp) => {
				console.log('resp', resp);
				// this.poll.comments.push()
				this.toastService.display('commentaire ajouté');
			})
			.catch((error) => {
				this.toastService.display('Erreur', error.message);
			});
	}
}
