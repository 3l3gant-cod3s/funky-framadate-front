import { Component, Input, OnInit } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';

import { ToastService } from '../../../../core/services/toast.service';

@Component({
	selector: 'app-copy-text',
	templateUrl: './copy-text.component.html',
	styleUrls: ['./copy-text.component.scss'],
})
export class CopyTextComponent implements OnInit {
	@Input() public textToCopy: string;
	public displayContentToCopy = false;
	@Input() public displayLabelButton = true;

	constructor(private _clipboardService: ClipboardService, private toastService: ToastService) {}

	ngOnInit(): void {}

	public copy(): void {
		this._clipboardService.copyFromContent(this.textToCopy);
		this.toastService.display(`Texte copié : ${this.textToCopy}`);
	}
}
