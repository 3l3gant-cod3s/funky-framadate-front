import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../../../core/services/storage.service';
import { ChoiceText } from '../../../../core/models/choice.model';

@Component({
	selector: 'app-option-link',
	templateUrl: './option-link.component.html',
	styleUrls: ['./option-link.component.scss'],
})
export class OptionLinkComponent implements OnInit {
	public url_href: string;
	public url_display: string;
	public choice_for_modal: ChoiceText; // choice to be modified after validation of modal
	public display_option_dialog: boolean = false;

	constructor(public StorageService: StorageService) {}

	ngOnInit(): void {}

	openLinkModal(choice: ChoiceText) {
		this.choice_for_modal = choice;
		this.display_option_dialog = true;
	}

	addLink() {
		this.StorageService.choicesText.push(new ChoiceText());
	}

	validateModal() {
		this.choice_for_modal.url_href = '' + this.url_href;
		this.choice_for_modal.url_display = '' + this.url_display;
		this.display_option_dialog = false;
		this.url_href = '';
		this.url_display = '';
	}

	closeModal() {
		this.display_option_dialog = false;
	}
}
