import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';

import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DOCUMENT } from '@angular/common';
import { TimeSlices } from '../../../../../../core/models/dateChoice.model';

@Component({
	selector: 'app-time-list',
	templateUrl: './time-list.component.html',
	styleUrls: ['./time-list.component.scss'],
})
export class TimeListComponent implements OnInit {
	@Input()
	public timeSlices: TimeSlices[];
	@Input()
	public prefix_choice_id = '';

	constructor(@Inject(DOCUMENT) private document: any, private cd: ChangeDetectorRef) {}

	ngOnInit(): void {}

	dropTimeItem(event: any): void {
		// moveItemInArray(this.timeSlices, event.previousIndex, event.currentIndex);
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} else {
			transferArrayItem(
				event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex
			);
		}
	}

	removeTime(id: number) {
		this.timeSlices.splice(id, 1);
		this.focusOnLastField();
	}

	focusOnFieldNumber(fieldId: number) {
		console.log('focus on fieldId', fieldId);
		const selector = '#choice_' + this.prefix_choice_id + '_timeChoices_' + fieldId;
		const firstField = this.document.querySelector(selector);
		if (firstField) {
			console.log('found', firstField);
			firstField.focus();
		}
		return firstField;
	}

	focusOnNextField(currentId: number) {
		const fieldFound = this.focusOnFieldNumber(currentId + 1);
		if (!fieldFound) {
			console.log('pas trouvé de field avec id', currentId + 1);
			this.createNewField();
		}
	}

	focusOnLastField() {
		this.cd.detectChanges();
		if (!this.focusOnFieldNumber(this.timeSlices.length - 1)) {
			console.log('no last time timeSlice found');
			this.createNewField();
			this.focusOnLastField();
		}
	}

	removeTimeIfEmpty(timeLiteral, id) {
		if (timeLiteral === '') {
			this.removeTime(id);
			this.focusOnFieldNumber(0);
		}
	}

	createNewField() {
		// create new field
		this.timeSlices.push({
			literal: '',
		});
		this.cd.detectChanges();
	}
}
