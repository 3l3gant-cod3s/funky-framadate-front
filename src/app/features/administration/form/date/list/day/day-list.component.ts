import { ChangeDetectorRef, Component, Inject, Input } from '@angular/core';
import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DOCUMENT } from '@angular/common';
import { FormGroup } from '@angular/forms';
import { ToastService } from '../../../../../../core/services/toast.service';
import { StorageService } from '../../../../../../core/services/storage.service';
import { MatDialog } from '@angular/material/dialog';
import { ShortcutsHelpComponent } from '../../../../../shared/components/ui/shortcuts-help/shortcuts-help.component';
import { DateChoice } from '../../../../../../core/models/dateChoice.model';
import { PollService } from '../../../../../../core/services/poll.service';
import { DateUtilitiesService } from '../../../../../../core/services/date.utilities.service';
import { environment } from 'src/environments/environment';
@Component({
	selector: 'app-day-list',
	templateUrl: './day-list.component.html',
	styleUrls: ['./day-list.component.scss'],
	entryComponents: [ShortcutsHelpComponent],
})
export class DayListComponent {
	@Input()
	form: FormGroup;
	public dateChoices: DateChoice[] = [];
	@Input()
	public hasSeveralHours: boolean;
	timeList: any;
	display = false;
	public environment = environment;

	constructor(
		public dialog: MatDialog,
		private toastService: ToastService,
		private pollService: PollService,
		private dateUtilitiesService: DateUtilitiesService,
		private cd: ChangeDetectorRef,
		@Inject(DOCUMENT) private document: any,
		private storageService: StorageService
	) {
		this.dateChoices = this.pollService.dateChoiceList;
	}

	reinitChoices(): void {
		this.dateChoices = [];
	}

	setDemoTextChoices(): void {
		this.addChoice('orange');
		this.addChoice('raisin');
		this.addChoice('abricot');
	}

	dropDayItem(event: any): void {
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} else {
			transferArrayItem(
				event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex
			);
		}
	}

	/**
	 * add a time period to a specific date timeSlice,
	 * focus on the new input
	 * @param choice DateChoice
	 * @param id number
	 */
	addTimeToDate(choice: DateChoice, id: number): void {
		if (!choice.timeSlices) {
			choice.timeSlices = [];
		}
		choice.timeSlices.push({
			literal: '',
		});
		// focus on created field
		this.cd.detectChanges();
		const selector = '#choice_' + id + '_timeChoices_' + (choice.timeSlices.length - 1);
		const firstField = this.document.querySelector(selector);
		if (firstField) {
			firstField.focus();
		} else {
			console.log('no last time timeSlice found');
		}
	}

	/**
	 * handle keyboard shortcuts
	 * @param $event
	 * @param choice_number
	 */
	keyOnChoice($event: KeyboardEvent, choice_number: number): void {
		$event.preventDefault();

		console.log('this. dateChoices.length', this.dateChoices.length);
		console.log('choice_number', choice_number);
		const lastChoice = this.dateChoices.length - 1 === choice_number;
		// TODO handle shortcuts
		// reset field with Ctrl + D
		// add a field with Ctrl + N
		// go to previous timeSlice with arrow up
		// go to next timeSlice with arrow down
		console.log('$event', $event);

		if ($event.ctrlKey && $event.key == 'ArrowUp' && choice_number > 0) {
			this.focusOnChoice(choice_number - 1);
		}
		if ($event.ctrlKey && ($event.key == 'ArrowDown' || $event.key == 'ArrowRight')) {
			// add a field if we are on the last timeSlice
			if (lastChoice) {
				this.addChoice();
				this.toastService.display('choix ajouté par raccourci "flèche bas"');
			} else {
				this.focusOnChoice(choice_number + 1);
			}
		}
		if ($event.ctrlKey && $event.key == 'Backspace') {
			this.deleteChoiceField(choice_number);
			this.toastService.display('choix supprimé par raccourci "Ctrl + retour"');
			// this.cd.detectChanges();
			this.focusOnChoice(Math.min(choice_number - 1, 0));
		}
		if ($event.ctrlKey && $event.key == 'Enter') {
			// go to other fields
			const elem = this.document.querySelector('#creatorEmail');
			if (elem) {
				elem.focus();
			}
		}
	}

	addChoice(optionalLabel = ''): void {
		let lastDateChoice = this.pollService.dateChoiceList[this.pollService.dateChoiceList.length - 1];
		console.log('lastDateChoice', lastDateChoice);
		let lastDateChoiceObject = this.dateUtilitiesService.addDaysToDate(
			this.pollService.dateChoiceList.length,
			new Date()
		);

		if (lastDateChoice && lastDateChoice.date_object) {
			lastDateChoiceObject = lastDateChoice.date_object;
		}
		this.pollService.dateChoiceList.push(
			this.dateUtilitiesService.convertDateToDateChoiceObject(
				this.dateUtilitiesService.addDaysToDate(1, lastDateChoiceObject)
			)
		);
		this.pollService.dateChoiceList.sort((a: any, b: any) => {
			return a.date_object - b.date_object;
		});
		this.cd.detectChanges();
		this.focusOnChoice(this.pollService.dateChoiceList.length - 1);
	}

	focusOnChoice(index): void {
		const selector = '#dateChoices_' + index;
		const elem = this.document.querySelector(selector);
		console.log('focusOnChoice elem', selector, elem);
		if (elem) {
			elem.focus();
		}
	}

	deleteChoiceField(index: number): void {
		this.dateChoices.splice(index, 1);
		this.cd.detectChanges();
		this.focusOnChoice(index - 1 < 0 ? 0 : index - 1);
	}

	isWeekendDay(date_input: string) {
		let date_object = new Date(Date.parse(date_input));
		if (date_object) {
			const day = date_object.getDay();
			return day === 6 || day === 0;
		}
		return false;
	}

	openSimple() {
		this.display = !this.display;
	}

	/**
	 * convert back date object to datechoices
	 * @param date_as_string
	 * @param choice_id
	 */
	convertDateInput(date_as_string: string, choice_id: number) {
		console.log('$event,date_object', date_as_string, choice_id);
		let newDate = new Date(date_as_string);
		this.dateChoices[choice_id].date_object = newDate;
		this.dateChoices[choice_id].date_input = date_as_string;
	}
}
