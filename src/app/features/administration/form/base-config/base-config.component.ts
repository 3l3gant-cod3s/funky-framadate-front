import { ChangeDetectorRef, Component, Inject, Input } from '@angular/core';
import { ToastService } from '../../../../core/services/toast.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UuidService } from '../../../../core/services/uuid.service';
import { PollService } from '../../../../core/services/poll.service';
import { ApiService } from '../../../../core/services/api.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Poll } from '../../../../core/models/poll.model';
import { environment } from '../../../../../environments/environment';
import { PollUtilitiesService } from '../../../../core/services/poll.utilities.service';

@Component({
	selector: 'app-base-config',
	templateUrl: './base-config.component.html',
	styleUrls: ['./base-config.component.scss'],
})
export class BaseConfigComponent {
	@Input()
	public poll?: Poll;
	@Input()
	public form: FormGroup;

	constructor(
		private fb: FormBuilder,
		private cd: ChangeDetectorRef,
		private uuidService: UuidService,
		private toastService: ToastService,
		private pollService: PollService,
		private utilitiesService: PollUtilitiesService,
		private apiService: ApiService,
		private router: Router,
		@Inject(DOCUMENT) private document: Document
	) {}

	public updateSlug(): void {
		const newValueFormatted = this.pollService.makeSlug(this.pollService.form);
		console.log('newValueFormatted', newValueFormatted);
		this.form.patchValue({ custom_url: newValueFormatted });
	}

	getErrorMessage(fieldControl) {
		return fieldControl.hasError('required')
			? 'You must enter a value'
			: fieldControl.hasError('email')
			? 'Not a valid email'
			: '';
	}
}
