import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PollService } from '../../../../../core/services/poll.service';
import { environment } from '../../../../../../environments/environment';

@Component({
	selector: 'app-step-five',
	templateUrl: './step-five.component.html',
	styleUrls: ['./step-five.component.scss'],
})
export class StepFiveComponent implements OnInit {
	@Input() step_max: any;
	@Input() public form: FormGroup;
	poll: any;
	public environment = environment;
	advancedDisplayEnabled = environment.advanced_options_display;

	constructor(public pollService: PollService) {
		this.pollService.step_current = 5;
	}

	ngOnInit(): void {}

	askInitFormDefault() {
		if (window.confirm('réinitialiser le formulaire ?')) {
			this.pollService.askInitFormDefault();
		}
	}

	automaticSlug() {}
}
