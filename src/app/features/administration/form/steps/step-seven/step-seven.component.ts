import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PollService } from '../../../../../core/services/poll.service';

@Component({
	selector: 'app-step-seven',
	templateUrl: './step-seven.component.html',
	styleUrls: ['./step-seven.component.scss'],
})
export class StepSevenComponent implements OnInit {
	constructor(private router: Router, public pollService: PollService) {
		this.pollService.step_current = 7;
	}

	ngOnInit(): void {}
}
