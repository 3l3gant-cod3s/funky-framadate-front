import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';
import { UuidService } from '../../../../../core/services/uuid.service';
import { ToastService } from '../../../../../core/services/toast.service';
import { PollService } from '../../../../../core/services/poll.service';
import { DateUtilitiesService } from '../../../../../core/services/date.utilities.service';
import { ApiService } from '../../../../../core/services/api.service';
import { environment } from '../../../../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-step-two',
	templateUrl: './step-two.component.html',
	styleUrls: ['./step-two.component.scss'],
})
export class StepTwoComponent implements OnInit {
	ngOnInit(): void {}

	@Input()
	form: any;
	@Input()
	step_max: any;
	timeList: any;
	allowSeveralHours: string;
	dateList: any;
	showDateInterval: boolean;
	intervalDays: any;

	constructor(
		private fb: FormBuilder,
		private cd: ChangeDetectorRef,
		private uuidService: UuidService,
		private toastService: ToastService,
		public pollService: PollService,
		private router: Router,
		private titleService: Title,
		private translate: TranslateService,
		public dateUtilities: DateUtilitiesService,
		private apiService: ApiService,
		@Inject(DOCUMENT) private document: any
	) {
		this.form = this.pollService.form;
		this.pollService.step_current = 2;
		this.step_max = this.pollService.step_max;
	}

	get choices(): FormArray {
		return this.form.get('choices') as FormArray;
	}

	addTime() {}

	removeAllTimes() {}

	resetTimes() {}

	addChoice() {}

	addTimeToDate(choice: any, id: number) {}

	countDays() {}
}
