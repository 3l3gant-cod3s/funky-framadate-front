import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { PollService } from '../../../../../core/services/poll.service';
import { environment } from '../../../../../../environments/environment';
import { Router } from '@angular/router';
import { DateUtilitiesService } from '../../../../../core/services/date.utilities.service';
import { DOCUMENT } from '@angular/common';
import { StorageService } from '../../../../../core/services/storage.service';
import { DateChoice, TimeSlices, timeSlicesProposals } from '../../../../../core/models/dateChoice.model';
import { ToastService } from '../../../../../core/services/toast.service';

@Component({
	selector: 'app-step-four',
	templateUrl: './step-four.component.html',
	styleUrls: ['./step-four.component.scss'],
})
export class StepFourComponent implements OnInit {
	public urlPrefix: any;
	public environment = environment;
	@Input()
	step_max: any;
	@Input()
	form: any;
	showSameTimeSlices: boolean = false;
	timeSlicesProposals: TimeSlices[] = timeSlicesProposals;
	display_same_hours_dialog: boolean = false;

	constructor(
		private dateUtilitiesService: DateUtilitiesService,
		private router: Router,
		private toastService: ToastService,
		private cd: ChangeDetectorRef,
		@Inject(DOCUMENT) private document: any,
		private storageService: StorageService,
		public pollService: PollService
	) {
		this.pollService.step_current = 4;
	}

	ngOnInit(): void {}

	/**
	 * toggle hasSeveralHours to show an other form section
	 * so that the user can choose between time slices applying equally to all days in her form,
	 * or have the ability to define different times lices for each day
	 */
	toggleHasSeveralHours() {
		this.pollService.form.patchValue({
			hasSeveralHours: !this.pollService.form.value.hasSeveralHours,
		});
	}

	addChoiceForDay(dayChoice: DateChoice): void {
		let lastDateChoice = this.pollService.dateChoiceList[this.pollService.dateChoiceList.length];
		console.log('lastDateChoice', lastDateChoice);
		let lastDateChoiceObject = this.dateUtilitiesService.addDaysToDate(
			this.pollService.dateChoiceList.length,
			new Date()
		);

		if (lastDateChoice && lastDateChoice.date_object) {
			lastDateChoiceObject = lastDateChoice.date_object;
		} else {
			lastDateChoiceObject = new Date();
		}
		dayChoice.timeSlices.push({
			literal: '',
		});
		dayChoice.timeSlices.sort((a: any, b: any) => {
			return a.date_object - b.date_object;
		});
		this.focusOnChoice(this.storageService.dateChoices.length - 1);
	}

	focusOnChoice(index): void {
		const selector = '#choice_label_' + index;
		const elem = this.document.querySelector(selector);
		if (elem) {
			elem.focus();
		}
	}

	addPreselect(literal: string) {
		if (
			!this.pollService.timeList.find((elem) => {
				return elem.literal === literal;
			})
		) {
			this.pollService.timeList.push({ literal });
		}
	}

	applyTimeSlicesToDateChoices() {
		let timeSlicesToApply = this.pollService.timeList;
		this.pollService.dateChoiceList.forEach((elem) => {
			return (elem.timeSlices = Object.create(timeSlicesToApply));
		});
		this.closeModalAndFocusOnOpenModalButton();
	}

	/**
	 * on modal close, focus on the close button for accessibility
	 */
	closeModalAndFocusOnOpenModalButton() {
		this.display_same_hours_dialog = false;
		let buttonClose = this.document.querySelector('#apply_same_hour');
		if (buttonClose) {
			buttonClose.focus();
		}
	}

	/**
	 * open the modal to set the same timeslices to all days
	 */
	openModal() {
		this.display_same_hours_dialog = true;
		this.cd.detectChanges();
		let buttonClose = this.document.querySelector('#close_dialog');
		console.log('openModal', buttonClose);
		if (buttonClose) {
			buttonClose.focus();
		}
	}
}
