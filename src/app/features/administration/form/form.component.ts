import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { Poll } from '../../../core/models/poll.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UuidService } from '../../../core/services/uuid.service';
import { ApiService } from '../../../core/services/api.service';
import { ToastService } from '../../../core/services/toast.service';
import { PollService } from '../../../core/services/poll.service';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'app-admin-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
	@Input()
	public poll?: Poll;
	public form: FormGroup;

	constructor(
		private fb: FormBuilder,
		private cd: ChangeDetectorRef,
		private uuidService: UuidService,
		private toastService: ToastService,
		public pollService: PollService,
		private router: Router,
		public route: ActivatedRoute,
		private apiService: ApiService,
		@Inject(DOCUMENT) private document: any
	) {
		this.form = this.pollService.form;
	}

	ngOnInit(): void {
		if (environment.autofill_creation) {
			this.pollService.askInitFormDefault();
		}
	}

	goNextStep() {}
}
