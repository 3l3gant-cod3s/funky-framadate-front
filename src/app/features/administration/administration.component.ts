import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Poll } from '../../core/models/poll.model';
import { DOCUMENT } from '@angular/common';
import { slideInAnimation } from '../../shared/animations/main';

@Component({
	selector: 'app-administration',
	templateUrl: './administration.component.html',
	styleUrls: ['./administration.component.scss'],
	animations: [slideInAnimation],
})
export class AdministrationComponent implements OnInit, OnDestroy {
	public poll: Poll;
	private routeSubscription: Subscription;

	constructor(private route: ActivatedRoute, @Inject(DOCUMENT) private document: any) {}

	ngOnInit(): void {
		this.routeSubscription = this.route.data.subscribe((data: { poll: Poll }) => {
			console.log('routeSubscription data', data);
			if (data.poll) {
				this.poll = data.poll;
			}
		});
	}

	ngOnDestroy(): void {
		if (this.routeSubscription) {
			this.routeSubscription.unsubscribe();
		}
	}
}
