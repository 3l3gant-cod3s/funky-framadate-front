import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '../../../core/services/toast.service';

@Component({
	selector: 'app-stepper',
	templateUrl: './stepper.component.html',
	styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent implements OnInit {
	@Input()
	public stepperConfirm: HTMLElement;

	@Input()
	public step_current: number = 1;
	@Input()
	public step_max: number = 5;
	public environment = environment;
	display_cancel_dialog: boolean = false;
	display_mobile_menu: boolean = false;

	constructor(
		public pollService: PollService,
		@Inject(DOCUMENT) private document: any,
		private cd: ChangeDetectorRef,
		private titleService: Title,
		private toastService: ToastService,
		private translate: TranslateService,
		private router: Router
	) {}
	ngOnInit() {}

	showCancelDialog() {
		this.display_cancel_dialog = true;
		this.cd.detectChanges();
		let buttonClosepopup = this.document.querySelector('#close_dialog');
		if (buttonClosepopup) {
			buttonClosepopup.focus();
			console.log('button close found');
		} else {
			console.log('not found');
		}
	}

	focusOnCancelButton() {
		this.display_cancel_dialog = false;
		let buttonClose = this.document.querySelector('#display_cancel_popup_button');
		if (buttonClose) {
			buttonClose.focus();
		}
	}

	goToHome() {
		this.display_cancel_dialog = false;
		this.router.navigate(['/']);
	}

	toggle_mobile_menu() {
		this.display_mobile_menu = !this.display_mobile_menu;
	}
}
