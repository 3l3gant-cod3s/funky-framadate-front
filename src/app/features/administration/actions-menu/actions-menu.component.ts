import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PollUtilitiesService } from '../../../core/services/poll.utilities.service';
import { StorageService } from '../../../core/services/storage.service';
import { ApiService } from '../../../core/services/api.service';
import { PollService } from '../../../core/services/poll.service';
import { DateService } from '../../../core/services/date.service';
import { ToastService } from '../../../core/services/toast.service';
import { ConfirmationService } from 'primeng/api';

@Component({
	selector: 'app-actions-menu',
	templateUrl: './actions-menu.component.html',
	styleUrls: ['./actions-menu.component.scss'],
})
export class ActionsMenuComponent implements OnInit {
	@Input() displayMenu = false;

	constructor(
		private router: Router,
		private utils: PollUtilitiesService,
		private _Activatedroute: ActivatedRoute,
		private confirmationService: ConfirmationService,
		public storageService: StorageService,
		public api: ApiService,
		public pollService: PollService,
		public dateService: DateService,
		public toastService: ToastService
	) {}

	ngOnInit(): void {}

	/**
	 * export all the poll data available to the public as a CSV single file
	 */
	exportCSV(): void {
		this.utils.exportCSV(this.pollService._poll.getValue());
	}

	exportJson(): void {
		this.utils.download(
			'export_poll_' + this.pollService._poll.getValue().custom_url + '.json',
			JSON.stringify(this.pollService._poll.getValue())
		);
	}

	duplicate(): void {
		alert('TODO');
	}

	print(): void {
		alert('TODO');
	}

	deleteAllVotes() {
		this.confirmationService.confirm({
			message: 'Supprimer tous les votes de ce sondage?',
			accept: () => {
				alert('TODO');
				console.log('TODO');
			},
		});
	}

	deleteAllComments() {
		alert('TODO');
		console.log('TODO');
	}

	deletePoll() {
		alert('TODO');
		console.log('TODO');
	}
}
