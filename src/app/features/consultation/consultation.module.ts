import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';
import { ConsultationRoutingModule } from './consultation-routing.module';
import { ConsultationComponent } from './consultation.component';
import { PollResultsCompactComponent } from './poll-results-compact/poll-results-compact.component';
import { PollResultsDetailedComponent } from './poll-results-detailed/poll-results-detailed.component';
import { ChoiceButtonComponent } from '../../shared/components/choice-item/choice-button.component';
import { PasswordPromptComponent } from './password/password-prompt/password-prompt.component';
import { ConsultationLandingComponent } from './consultation-landing/consultation-landing.component';
import { ConsultationUserComponent } from './consultation-user/consultation-user.component';
import { SuccessComponent } from './success/success.component';
import { AdministrationModule } from '../administration/administration.module';
import { EditComponent } from './edit/edit.component';
import { ResultsRoundedComponent } from './results-rounded/results-rounded.component';
import { PollResultsDinumComponent } from './poll-results-dinum/poll-results-dinum.component';
import { ChoiceTableComponent } from './choice-table/choice-table.component';

@NgModule({
	declarations: [
		ConsultationComponent,
		PollResultsCompactComponent,
		PollResultsDetailedComponent,
		ChoiceButtonComponent,
		PasswordPromptComponent,
		ConsultationLandingComponent,
		ConsultationUserComponent,
		SuccessComponent,
		EditComponent,
		ResultsRoundedComponent,
		PollResultsDinumComponent,
		ChoiceTableComponent,
	],
	imports: [
		CommonModule,
		ConsultationRoutingModule,
		SharedModule,
		TranslateModule.forChild({ extend: true }),
		AdministrationModule,
	],
})
export class ConsultationModule {}
