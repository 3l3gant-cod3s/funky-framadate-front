import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ToastService } from '../../../core/services/toast.service';
import { StorageService } from '../../../core/services/storage.service';
import { PollService } from '../../../core/services/poll.service';
import { Poll } from '../../../core/models/poll.model';
import { PollUtilitiesService } from '../../../core/services/poll.utilities.service';
import { ApiService } from '../../../core/services/api.service';
import { DateService } from '../../../core/services/date.service';

@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
	public poll: Poll;
	public fetching = true;
	private pollSlug: string;
	private pass_hash: string;

	constructor(
		private router: Router,
		private utils: PollUtilitiesService,
		private _Activatedroute: ActivatedRoute,
		public storageService: StorageService,
		public api: ApiService,
		public pollService: PollService,
		public dateService: DateService,
		public toastService: ToastService
	) {
		this.pollService.poll.subscribe((newpoll: Poll) => {
			console.log('newpoll', newpoll);
			this.poll = newpoll;
		});
	}

	ngOnInit(): void {
		this._Activatedroute.paramMap.subscribe((params: ParamMap) => {
			console.log('params _Activatedroute', params);
			this.pollSlug = params.get('custom_url');
			this.pass_hash = params.get('pass_hash');

			console.log('this.pass_hash ', this.pass_hash);
			if (this.pass_hash) {
				this.pollService.loadPollByCustomUrlWithPasswordHash(this.pollSlug, this.pass_hash).then((resp) => {
					console.log('loadPollByCustomUrlWithPasswordHash resp', this.pollService._poll.getValue());
					this.fetching = false;
					this.storageService.vote_stack.id = null;
					this.storageService.setChoicesForVoteStack(this.pollService._poll.getValue().choices);
				});
			} else {
				this.pollService.loadPollByCustomUrl(this.pollSlug).then((resp) => {
					console.log('loadPollByCustomUrl resp', resp);
					this.fetching = false;
					this.storageService.vote_stack.id = null;
					this.storageService.setChoicesForVoteStack(this.pollService._poll.getValue().choices);
				});
			}
		});
	}

	/**
	 * make a date from an ISO string
	 * @param s
	 */
	dateFromString(s: string) {
		return new Date(s);
	}

	dayStringFromDateString(date_string: string): string {
		let date = this.dateFromString(date_string);
		return (
			'calendar_widget.dayNames.' +
			date.toLocaleDateString('en-US', {
				weekday: 'long',
				year: 'numeric',
				month: 'short',
				day: 'numeric',
			})
		);
	}
}
