import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
	MissingTranslationHandler,
	MissingTranslationHandlerParams,
	TranslateLoader,
	TranslateModule,
	TranslateService,
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ClipboardModule } from 'ngx-clipboard';
import { MarkdownModule } from 'ngx-markdown';
import { NgxWebstorageModule } from 'ngx-webstorage';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { CguComponent } from './features/shared/components/ui/static-pages/cgu/cgu.component';
import { LegalComponent } from './features/shared/components/ui/static-pages/legal/legal.component';
import { PrivacyComponent } from './features/shared/components/ui/static-pages/privacy/privacy.component';
import { CipheringComponent } from './features/shared/components/ui/static-pages/ciphering/ciphering.component';
import { ErrorsListComponent } from './features/shared/components/ui/form/errors-list/errors-list.component';
import { KeyboardShortcutsModule } from 'ng-keyboard-shortcuts';
import { AdministrationModule } from './features/administration/administration.module';
import { EditComponent } from './features/consultation/edit/edit.component';
import { ChoiceButtonDinumComponent } from './features/shared/components/choice-button-dinum/choice-button-dinum.component';
import { StaticPagesComponent } from './features/shared/static-pages/static-pages.component';

// register languages files for translation
// import localeEn from '@angular/common/locales/en';
// import localeFr from '@angular/common/locales/fr';
// import localeEs from '@angular/common/locales/es';
// // code for locale are listed by the ISO-639
// registerLocaleData(localeFr, 'fr-FR');
// registerLocaleData(localeEn, 'en-EN');
// registerLocaleData(localeEs, 'ca-ES');

export class MyMissingTranslationHandler implements MissingTranslationHandler {
	public handle(params: MissingTranslationHandlerParams): string {
		return `MISSING TRANSLATION FOR [${params.key}]`;
	}
}

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [
		AppComponent,
		CguComponent,
		LegalComponent,
		PrivacyComponent,
		CipheringComponent,
		StaticPagesComponent,
	],
	imports: [
		AppRoutingModule,
		AdministrationModule,
		BrowserAnimationsModule,
		BrowserModule,
		ClipboardModule,
		CommonModule,
		CoreModule,
		FormsModule,
		HttpClientModule,
		KeyboardShortcutsModule.forRoot(),
		MarkdownModule.forRoot(),
		NgxWebstorageModule.forRoot({ prefix: environment.localStorage.key }),
		SharedModule,
		TranslateModule.forRoot({
			defaultLanguage: environment.defaultLanguage,
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient],
			},
			missingTranslationHandler: {
				provide: MissingTranslationHandler,
				useClass: MyMissingTranslationHandler,
			},
			useDefaultLang: true,
		}),
	],
	providers: [Title, TranslateService],
	bootstrap: [AppComponent],
	exports: [ErrorsListComponent, ChoiceButtonDinumComponent],
})
export class AppModule {}
