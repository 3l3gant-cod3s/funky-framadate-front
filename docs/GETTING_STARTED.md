# Pour débuter - getting started
Clonez le dépot, installez les dépendances, et lancez le serveur local qui se recharge lorsque vous sauvegardez des fichiers dans le dépot. Les fois suivantes vous n'aurez qu'a lancer yarn start. 
```bash
# clone the repo
git clone https://framagit.org/framasoft/framadate/funky-framadate-front.git
cd funky-framadate-front
# install yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

yarn install --pure-lockfile
yarn start
# and now check your browser http://localhost:4200
```

Pour interagir avec la base de données, il vous faudra également faire démarrer l'API Symfony de backend dans l'autre dépot, qui se lance par défaut sur le port 8000.

# Pas de config docker
Nous n'avons pas actuellement de config docker qui solutionnerait tout ça, les merge request sont les bienvenues! :)
